﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;

namespace SheakerSort
{
    class DGV
    {
        public DGV()
        {

        }
        public void CreateMainDgv(int c, DataGridView dgv)
        {
            for (int i = 0; i < c; i++)
            {
                dgv.Columns.Add("", $"{i}");
                
            }
        }


        public static void SetDgv(int a, DataGridView dgv, int r, int c)
        {
            dgv.Rows[r].Cells[c].Value = Convert.ToString(a);
            
        }

        public void ArrayToDgv(int[] arr, DataGridView dgv, int r, int column, int min, int max)
        {
            Random rand = new Random();
            for (int i = 0; i < arr.Length; i++)
            {
                arr[i] = rand.Next(min, max + 1);
                SetDgv(arr[i], dgv, r, column);
                column++;    
            }
        }
    }
}
