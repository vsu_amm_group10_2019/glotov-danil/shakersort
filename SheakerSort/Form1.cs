﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SheakerSort
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
           /* DGV dgv = new DGV();
            dgv.CreateMainDgv(0, dataGridView1);*/
        }
        
        
        private void button1_Click(object sender, EventArgs e)
        {
            DGV dgv = new DGV();
            int c;
            int max;
            int min;
            c = Convert.ToInt32(textBoxCount.Text);
            max = Convert.ToInt32(textBoxMax.Text);
            min = Convert.ToInt32(textBoxMin.Text);

            var array = new int[c];


            dgv.CreateMainDgv(c, dataGridView1);
            dgv.ArrayToDgv(array, dataGridView1, 0, 0, min, max);
            Sort.ShakerSort(array, dataGridView1);
            dataGridView1.Refresh();

        }

        private void button2_Click(object sender, EventArgs e)
        {
            GraphicsData data = new GraphicsData();

            this.chartCompare.Series[0].Points.Clear();
            int x = 0, y = 0, b = 1000;
            while (x < b)
            {
                y = data.ShakerSort(data.CreateArray(x), "compare");
                this.chartCompare.Series[0].Points.AddXY(x, y);
                y = data.ShakerSort(data.CreateArray(x), "swap");
                this.chartSwap.Series[0].Points.AddXY(x, y);
                x++;
            }
        }
    }
    
}
