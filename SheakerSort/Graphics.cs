﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SheakerSort
{
    class GraphicsData
    {



        //метод обмена элементов
        public void Swap(ref int e1, ref int e2)
        {
            var temp = e1;
            e1 = e2;
            e2 = temp;
        }

        //сортировка перемешиванием
        public int ShakerSort(int[] array, string str)
        {
            int countCompare;
            int countSwap;
            countCompare = 0;
            countSwap = 0;
            for (var i = 0; i < array.Length / 2; i++)
            {
                var swapFlag = false;
                //проход слева направо
                for (var j = i; j < array.Length - i - 1; j++)
                {
                    countCompare++;
                    if (array[j] > array[j + 1])
                    {
                        countSwap++;
                        Swap(ref array[j], ref array[j + 1]);
                        swapFlag = true;
                    }
                }

                //проход справа налево
                for (var j = array.Length - 2 - i; j > i; j--)
                {
                    countCompare++;
                    if (array[j - 1] > array[j])
                    {
                        countSwap++;
                        Swap(ref array[j - 1], ref array[j]);
                        swapFlag = true;
                    }
                }

                //если обменов не было выходим
                if (!swapFlag)
                {

                    break;
                }
            }
            if (str == "compare")
            {
                return countCompare;
            }
            else if (str == "swap")
            {
                return countSwap;
            }
            else
            {
                return 0;
            }
        }
        public int[] CreateArray(int length)
        {
            var array = new int[length];
            Random rand = new Random();
            for (int i = 0; i < array.Length; i++)
            {
                array[i] = rand.Next(1, length+1);
            }
            return array;
        }
    }
}
