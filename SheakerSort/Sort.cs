﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;

namespace SheakerSort
{
    class Sort
    {
        int countCompare;
        int countSwap;
       
        
        //метод обмена элементов
        public static void Swap(ref int e1, ref int e2,int r, int c , DataGridView dgv )
        {
            ColorArrP(c, c + 1, r, System.Drawing.Color.Yellow, dgv);
            var temp = e1;
            e1 = e2;
            e2 = temp;
            dgv.Refresh();
            SetDgv(e2, dgv, r, c+1);
            SetDgv(e1, dgv, r, c);
            Thread.Sleep(400);
            dgv.Refresh();
            Thread.Sleep(400);
            ColorArrP(c, c + 1, r, System.Drawing.Color.White, dgv);
            dgv.Refresh();
        }

        //сортировка перемешиванием
        public static int[] ShakerSort(int[] array, DataGridView dgv)
        {
            
            for (var i = 0; i < array.Length / 2; i++)
            {
                var swapFlag = false;
                //проход слева направо
                for (var j = i; j < array.Length - i - 1; j++)
                {
                    //countCompare++;
                    if (array[j] > array[j + 1])
                    {   
                        //countSwap++;
                        Swap(ref array[j], ref array[j + 1], 0, j, dgv);
                        swapFlag = true;
                    }
                }

                //проход справа налево
                for (var j = array.Length - 2 - i; j > i; j--)
                {
                   // countCompare++;
                    if (array[j - 1] > array[j])
                    {
                       // countSwap++;
                        Swap(ref array[j - 1], ref array[j], 0, j - 1, dgv);
                        swapFlag = true;
                    }
                }

                //если обменов не было выходим
                if (!swapFlag)
                {
                    
                    break;
                }
            }
            ColorArrP(0, array.Length - 1, 0, System.Drawing.Color.LightGreen, dgv);
            return array;
        }

        public static void SetDgv(int a, DataGridView dgv, int r, int c)
        {
            dgv.Rows[r].Cells[c].Value = Convert.ToString(a);
        }


        public static void SetColor(int r, int c, System.Drawing.Color color ,DataGridView dgv)
        {
            dgv.Rows[r].Cells[c].Style.BackColor = color;
        }

        public static void ColorArrP(int firstIndex, int lastIndex, int row, System.Drawing.Color color , DataGridView dgv)
        {
            for (int c = firstIndex; c <= lastIndex; c++)
            {
                SetColor(row, c, color, dgv);
            }

        }

    }
}
